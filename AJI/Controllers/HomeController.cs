﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Http.Cors;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;


namespace AJI.Controllers
{
    [EnableCors(origins: "http://localhost:7843/ajiWebApi.asmx", headers: "*", methods: "*")]
    public class HomeController : Controller
    {        
        ajidbEntities1 aj = new ajidbEntities1();

        public ActionResult auAgua()
        {
            return View();
        }
        public ActionResult auAire()
        {
            return View();
        }
        public ActionResult auAnalisis()
        {
            return View();
        }
        public ActionResult auClimatizacion()
        {
            return View();
        }
        public ActionResult auEquipos()
        {            
            return View();
        }
        public ActionResult auTransporte()
        {
            return View();
        }

        public ActionResult listaEmpresas()
        {
            return View();
        }
        public ActionResult listaSitios()
        {
            return View();
        }
        public ActionResult listaAuditorias()
        {
            return View();
        }
        public ActionResult operaciones()
        {
            return View();
        }           
        public ActionResult Index()
        {            
            string user = "";
            try
            {
                user = Request.Params["User"].ToString(); var query = (from a in aj.usuarios
                                                                       where a.usuario1 == user
                                                                       select a).FirstOrDefault();
                var numEmpresas = (from a in aj.empresas
                                   where a.usuario.usuario1 == user
                                   select a).ToList();
                var numSitios = (from a in aj.sitios
                                 where a.empresa.usuario.usuario1 == user
                                 select a).ToList();
                ViewBag.Nenpresas = numEmpresas.Count.ToString();
                ViewBag.Nsitios = numSitios.Count.ToString();
                ViewBag.foto = query.foto;
                ViewBag.tipo = query.tipo;
                ViewBag.User = query.usuario1;
            }
            catch
            { }
            
            
            return View();
        }
         
        public ActionResult Login()
        {            
            return View();
        }
        
        public ActionResult Administrador()
        {
            string user = "";
            try
            {
                user = Request.Params["User"].ToString();
                var query = (from a in aj.usuarios
                             where a.usuario1 == user
                             select a).FirstOrDefault();
                var numEmpresas = (from a in aj.empresas
                                   where a.usuario.usuario1 == user
                                   select a).ToList();
                var numSitios = (from a in aj.sitios
                                 where a.empresa.usuario.usuario1 == user
                                 select a).ToList();
                ViewBag.Nenpresas = numEmpresas.Count.ToString();
                ViewBag.Nsitios = numSitios.Count.ToString();
                ViewBag.foto = query.foto;
                ViewBag.tipo = query.tipo;
                ViewBag.User = query.usuario1;
            }
            catch
            { }

            return View();
        }
        
        public ActionResult Usuario()
        {
            string user = "";
            try
            {
                user = Request.Params["User"].ToString();
                var query = (from a in aj.usuarios
                             where a.usuario1 == user
                             select a).FirstOrDefault();
                var numEmpresas = (from a in aj.empresas
                                   where a.usuario.usuario1 == user
                                   select a).ToList();
                var numSitios = (from a in aj.sitios
                                 where a.empresa.usuario.usuario1 == user
                                 select a).ToList();
                ViewBag.Nenpresas = numEmpresas.Count.ToString();
                ViewBag.Nsitios = numSitios.Count.ToString();
                ViewBag.foto = query.foto;
                ViewBag.tipo = query.tipo;
                ViewBag.User = query.usuario1;
            }
            catch
            { }

            return View();
        }
        
        public ActionResult Sitios()
        {
            string user = "";
            try
            {
                user = Request.Params["User"].ToString();
                var query = (from a in aj.usuarios
                             where a.usuario1 == user
                             select a).FirstOrDefault();
                var numEmpresas = (from a in aj.empresas
                                   where a.usuario.usuario1 == user
                                   select a).ToList();
                var numSitios = (from a in aj.sitios
                                 where a.empresa.usuario.usuario1 == user
                                 select a).ToList();
                ViewBag.Nenpresas = numEmpresas.Count.ToString();
                ViewBag.Nsitios = numSitios.Count.ToString();
                ViewBag.foto = query.foto;
                ViewBag.tipo = query.tipo;
                ViewBag.User = query.usuario1;
            }
            catch
            { }
            return View();
        }
        
        public ActionResult Auditoria()
        {
            string user = "";
            string rew = "";
            try
            {
                user = Request.Params["User"].ToString();
                try
                {
                    rew = Request.Params["review"].ToString();
                    var emp = rew.Split('/')[0];
                    var sit = rew.Split('/')[1];
                    ViewBag.nomEmpresa = emp;
                    ViewBag.nomSitio = sit;
                    ViewBag.imgEmpresa = (from a in aj.empresas
                                          where a.nombre == emp
                                          select a.logo).FirstOrDefault();
                }
                catch { }
                var query = (from a in aj.usuarios
                             where a.usuario1 == user
                             select a).FirstOrDefault();
                var numEmpresas = (from a in aj.empresas
                                   where a.usuario.usuario1 == user
                                   select a).ToList();
                var numSitios = (from a in aj.sitios
                                 where a.empresa.usuario.usuario1 == user
                                 select a).ToList();                
                ViewBag.foto = query.foto;
                ViewBag.tipo = query.tipo;
                ViewBag.Nenpresas = numEmpresas.Count.ToString();
                ViewBag.Nsitios = numSitios.Count.ToString();
                ViewBag.User = query.usuario1;
                ViewBag.Fecha = DateTime.Now.Date.ToString("yyyy-MM-dd");
                List<tipo> numbers = (from p in aj.tipoes select p).ToList();
                ViewBag.PolicyOrgs = numbers;
                try
                {
                    rew = Request.Params["code"].ToString();
                }
                catch { }
                ViewBag.Code = rew;
            }
            catch
            { }           

            return View();
        }
        
        public ActionResult Empresas()
        {
            string user = "";
            string review = "";
            try
            {
                user = Request.Params["User"].ToString();
                var query = (from a in aj.usuarios
                             where a.usuario1 == user
                             select a).FirstOrDefault();
                var numEmpresas = (from a in aj.empresas
                                   where a.usuario.usuario1 == user
                                   select a).ToList();
                var numSitios = (from a in aj.sitios
                                 where a.empresa.usuario.usuario1 == user
                                 select a).ToList();
                try
                {
                    review = Request.Params["review"].ToString();
                    if (review != "")
                    {
                        string st = chart_transporte(review);
                        string st3d = chart_energia(review);
                        ViewBag.dataBar = st.Remove(st.Length - 1, 1);
                        ViewBag.dataPie = st3d;
                    }
                }
                catch { }
                
                ViewBag.Nenpresas = numEmpresas.Count.ToString();
                ViewBag.Nsitios = numSitios.Count.ToString();
                ViewBag.foto = query.foto;
                ViewBag.tipo = query.tipo;
                ViewBag.User = query.usuario1;

            }
            catch
            { }
            return View();
        }
        //Para grafico de pastel
        public string chart_energia(string review)
        {


            var queryCharts = (from a in aj.sitios
                               where a.nombre_empresa == review
                               select a).FirstOrDefault();
            string valores = queryCharts.auditorias.OrderByDescending(
                a => a.fecha_ingreso).FirstOrDefault().descripcion;


            string valueTermico = valores.Split(',')[3].Split(':')[1];
            string valorelectrico = valores.Split(',')[4].Split(':')[1];
            string valorperdida = valores.Split(',')[5].Split(':')[1];
            string st = "{ x: 'Termico', text: 'Térmico', y: " + valueTermico + " }," +
                        "{ x: 'Electrico', text: 'Eléctrico', y: " + valorelectrico + " }," +
                        "{ x: 'Perdida', text: 'Perdida', y: " + valorperdida + " }";
            return st;
        }
        //Para grafico de barras
        public string chart_transporte(string review)
        {
            var queryCharts = (from a in aj.sitios
                               where a.nombre_empresa == review
                               select a.auditorias).FirstOrDefault();
            var queryOne = queryCharts.OrderByDescending(a => a.fecha_ingreso).
                FirstOrDefault().datos_transporte.
                FirstOrDefault().transportes.
                Where(a => a.tipo_combustible == "Diesel");
            string st = "";
            foreach (transporte t in queryOne)
            {
                st = st + "{ x: '" + t.marca_vehiculo + "', y: " + t.consumo_combustible + " }, ";
            }
            return st;
        }
        //peticiones json
        [HttpPost]
        public ActionResult authSession(string user)
        {
            var query="";
            try
            {
               query  = (from a in aj.usuarios
                             where a.usuario1 == user
                             select new { a.sesion }).FirstOrDefault().sesion.ToString();
            }
            catch { query = "false"; }
            return Json(query.ToLower(), JsonRequestBehavior.AllowGet);
        }
           
        [HttpPost]        
        public JsonResult LogOut(string user)
        {
            var result = (from r in aj.usuarios
                          where r.usuario1 == user
                          select r).FirstOrDefault();
            if (result != null)
            {
                result.sesion = false;
            }
            aj.SaveChanges();
            return Json("false", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Login(string user, string pass)
        {
            bool loged = false;
            var result = (from r in aj.usuarios
                          where r.usuario1 == user && r.contraseña == pass
                          select r).FirstOrDefault();
            if (result != null)
            {
                result.sesion = true;
                loged = true;
            }
            aj.SaveChanges();


            //JsonResult ft = saveEmpresa("save", 1, "sdsdsdsd", DateTime.Parse(DateTime.Now.ToShortDateString()), "sdasdasdas", null, "asdasdas", 1, "asdasdasdsa", "asdasdasdas");
            return Json(loged, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult saveEmpresa(string action ,string data)
        //public JsonResult saveEmpresa(string action,string nombre)
        {
            clCompany jsonModel = Newtonsoft.Json.JsonConvert.DeserializeObject<ICollection<clCompany>>(data).FirstOrDefault();

            empresa empCont = new empresa();
            bool done = false;
            if (action == "save")
            {
                empCont.id_usuario = aj.usuarios.Where(a => a.usuario1 == jsonModel.user).Select(a => a.id_usuario).FirstOrDefault();
                empCont.correo = jsonModel.correo;
                empCont.fecha_ingreso = DateTime.Parse(jsonModel.fecha_ingreso);
                empCont.localizacion = jsonModel.localizacion;
                empCont.logo = jsonModel.logo;
                empCont.nombre = jsonModel.nombre;
                empCont.num_fiscal = int.Parse(jsonModel.num_fiscal);
                empCont.representante = jsonModel.representante;
                empCont.telefono = jsonModel.telefono;
                aj.empresas.Add(empCont);
                aj.SaveChanges();
                done = true;
            }
            if (action == "update")
            {
                var original = aj.empresas.Where(a => a.nombre == jsonModel.nombreNuevo).FirstOrDefault();
                if (original != null)
                {
                    original.id_usuario = aj.usuarios.Where(a => a.usuario1 == jsonModel.user).Select(a => a.id_usuario).FirstOrDefault();
                    original.correo = jsonModel.correo;
                    original.fecha_ingreso = DateTime.Parse(jsonModel.fecha_ingreso);
                    original.localizacion = jsonModel.localizacion;
                    original.logo = jsonModel.logo;
                    original.nombre = jsonModel.nombre;
                    original.num_fiscal = int.Parse(jsonModel.num_fiscal);
                    original.representante = jsonModel.representante;
                    original.telefono = jsonModel.telefono;
                    aj.SaveChanges();
                    done = true;
                }
            }

            return Json(done, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public JsonResult saveSitio(string action,string data)
        {
            clSites jsonModel = Newtonsoft.Json.JsonConvert.DeserializeObject<ICollection<clSites>>(data).FirstOrDefault();
                        
            bool done = false;
            if (action == "save")
            {
                var em = (from emp in aj.empresas
                         where emp.nombre == jsonModel.nombreEmpresaParaSitio
                         select emp).FirstOrDefault();
                em.num_sitios = em.num_sitios + 1;
                sitio st = new sitio();
                st.id_empresa = aj.empresas.Where(a=>a.nombre == jsonModel.nombreEmpresaParaSitio).Select(a=>a.id_empresa).FirstOrDefault();
                st.area_utilizada = jsonModel.areaUtilizada;
                st.cogeneracion = jsonModel.cogeneracion;
                st.compensacion_auto_consumo = jsonModel.compensacionAutoconsumo;
                st.consumo_electrico_anual = jsonModel.consumoElectricoAnual;
                st.consumo_electrico_factura = jsonModel.consumoElectricoFactura;
                st.direccion = jsonModel.direccion;
                st.extrangero = int.Parse(jsonModel.extrangero);
                st.factor_mezcla = jsonModel.factorMezcla;
                st.geo_latitud = jsonModel.geoLatitud;
                st.geo_longitud = jsonModel.geoLongitud;
                st.instruccion = int.Parse(jsonModel.instruccion);
                st.nombre_empresa = jsonModel.nombreEmpresaParaSitio;
                st.nombre_sitio = jsonModel.nombreSitio;
                st.plazas_utilizadas = int.Parse(jsonModel.plazasUtilizadas);
                st.potencia_iluminacion = int.Parse(jsonModel.potenciaDeComputadora);
                st.potencia_pc = int.Parse(jsonModel.potenciaDeIluminacion);
                st.propiedad = jsonModel.Propiedad;
                st.proveedor = jsonModel.proveedor;
                st.suministrado_red = jsonModel.suministradoRed;
                st.tiempo_completo = int.Parse(jsonModel.tiempoCompleto);
                st.tiempo_parcial = int.Parse(jsonModel.tiempoParcial);
                st.tipo_uso = jsonModel.tipoUso;
                aj.sitios.Add(st);
                aj.SaveChanges();                
                done = true;
            }
            if (action == "update")
            {
                var st = (from r in aj.sitios
                              where r.nombre_empresa == jsonModel.nombreEmpresaParaSitio
                              && r.nombre_sitio == jsonModel.nombreSitioViejo
                              select r).FirstOrDefault();                 
                if (st != null)
                {
                    st.id_empresa = aj.empresas.Where(a => a.nombre == jsonModel.nombreEmpresaParaSitio).Select(a => a.id_empresa).FirstOrDefault();
                    st.area_utilizada = jsonModel.areaUtilizada;
                    st.cogeneracion = jsonModel.cogeneracion;
                    st.compensacion_auto_consumo = jsonModel.compensacionAutoconsumo;
                    st.consumo_electrico_anual = jsonModel.consumoElectricoAnual;
                    st.consumo_electrico_factura = jsonModel.consumoElectricoFactura;
                    st.direccion = jsonModel.direccion;
                    st.extrangero = int.Parse(jsonModel.extrangero);
                    st.factor_mezcla = jsonModel.factorMezcla;
                    st.geo_latitud = jsonModel.geoLatitud;
                    st.geo_longitud = jsonModel.geoLongitud;
                    st.instruccion = int.Parse(jsonModel.instruccion);
                    st.nombre_empresa = jsonModel.nombreEmpresaParaSitio;
                    st.nombre_sitio = jsonModel.nombreSitio;
                    st.plazas_utilizadas = int.Parse(jsonModel.plazasUtilizadas);
                    st.potencia_pc = int.Parse(jsonModel.potenciaDeComputadora);
                    st.potencia_iluminacion = int.Parse(jsonModel.potenciaDeIluminacion);
                    st.propiedad = jsonModel.Propiedad;
                    st.proveedor = jsonModel.proveedor;
                    st.suministrado_red = jsonModel.suministradoRed;
                    st.tiempo_completo = int.Parse(jsonModel.tiempoCompleto);
                    st.tiempo_parcial = int.Parse(jsonModel.tiempoParcial);
                    st.tipo_uso = jsonModel.tipoUso;

                    aj.SaveChanges();
                    done = true;
                }
            }
            return Json(done, JsonRequestBehavior.AllowGet);
        }

        
        //getsion de ususarios aji
        [HttpPost]
        public JsonResult getUsuarios()
        {
            var result = (from r in aj.usuarios
                          select new { r.foto, r.tipo, r.usuario1, r.contraseña });
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult getUsuario(string nombre)
        {
            var result = (from r in aj.usuarios
                          where r.usuario1 == nombre
                          select new { r.foto, r.tipo, r.usuario1, r.contraseña }).FirstOrDefault();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult crearUsuario(string action, string nombre, string tipo, string pass, string foto, string antiguoNombre)
        {
            var result = "";
            if (action == "crearUsuario")
            {
                var original = aj.usuarios.Where(a => a.usuario1 == antiguoNombre).FirstOrDefault();
                if (original != null)
                {

                    original.usuario1 = nombre;
                    original.tipo = tipo;
                    original.foto = foto;
                    original.contraseña = pass;
                    aj.SaveChanges();
                    result = "trueact";
                }
                else
                {
                    usuario us = new usuario();
                    us.usuario1 = nombre;
                    us.tipo = tipo;
                    us.foto = foto;
                    us.contraseña = pass;
                    aj.usuarios.Add(us);
                    aj.SaveChanges();
                    result = "true";
                }
            }
            return Json((result).ToString().ToLower(), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult eliminarUsuario(string nombre)
        {
            var result = false;
            var us = (from r in aj.usuarios
                      where r.usuario1 == nombre
                      select r).FirstOrDefault();
            aj.usuarios.Remove(us);
            aj.SaveChanges();
            result = true;
            return Json(result.ToString().ToLower(), JsonRequestBehavior.AllowGet);
        }
        ProcessToHelp pH = new ProcessToHelp();
        //Auditorias----------------------------------------------------------------
        [HttpPost]
        public JsonResult saveAuditoria(string sitio, string action, string dataAuditoria, string dataEquipos, string dataClimatizacion, string dataAire, string dataAnalisis, string dataTransporte, string dataAgua, string code)
        {
            bool done = false;//Respuesta de ejecucion de acciones
            
            /*Clase de auditoria*/
            clAuditoria dta = Newtonsoft.Json.JsonConvert.DeserializeObject<ICollection<clAuditoria>>(dataAuditoria).FirstOrDefault();
                        
            /*Llenado para equipos*/
            clEquipos eqModel = new clEquipos();
            if (action == "equipo" || action == "all")
            {
                eqModel = Newtonsoft.Json.JsonConvert.DeserializeObject<ICollection<clEquipos>>(dataEquipos).FirstOrDefault();
            }

            /*Llenado para climatizacion*/
            clClimatizacion cliModel = new clClimatizacion();
            if (action == "climatizacion" || action == "all")
            {
                cliModel = Newtonsoft.Json.JsonConvert.DeserializeObject<ICollection<clClimatizacion>>(dataClimatizacion).FirstOrDefault();
            }

            /*Llenado para analisis*/
            clAnalisis anModel = new clAnalisis();
            if (action == "analisis" || action == "all")
            {
                anModel = Newtonsoft.Json.JsonConvert.DeserializeObject<ICollection<clAnalisis>>(dataAnalisis).FirstOrDefault();
            }
            /*Llenado para transporter*/
            clTransporte trModel = new clTransporte();
            if (action == "transporte" || action == "all")
            {
                trModel = Newtonsoft.Json.JsonConvert.DeserializeObject<ICollection<clTransporte>>(dataTransporte).FirstOrDefault();
            }
            /*Llenado para transporter*/
            clAgua agModel = new clAgua();
            if (action == "agua" || action == "all")
            {
                agModel = Newtonsoft.Json.JsonConvert.DeserializeObject<ICollection<clAgua>>(dataAgua).FirstOrDefault();
            }
            clAire aiModel = new clAire();
            if (action == "aire" || action == "all")
            {
                aiModel = Newtonsoft.Json.JsonConvert.DeserializeObject<ICollection<clAire>>(dataAire).FirstOrDefault();
            }
            DateTime dtNw = DateTime.Parse(dta.fecha_ingreso);
            var aud = aj.auditorias.Where(a => a.codigo == code).FirstOrDefault();
            if (aud != null)
            {
                var climatizacion = aud.climatizacion.FirstOrDefault();
                var analisis = aud.analisis_cogeneracion.FirstOrDefault();
                var transportes = aud.datos_transporte.FirstOrDefault();
                var aguas = aud.aguas.FirstOrDefault();
                var aires = aud.aires.FirstOrDefault();
                if (eqModel.descripcion != ""
                    && eqModel.departamento != "" &&
                    eqModel.potencia != "" &&
                    eqModel.cantidad != "" &&
                    eqModel.horas_uso != "" &&
                    eqModel.consumo != "")
                {
                    try
                    {
                        int id = int.Parse(eqModel.id_Equipo);
                        var equipos = aud.equipoes.Where(a => a.id_equipo == id).FirstOrDefault();
                        if (action == "equipo" || action == "all")
                        {
                            equipos = pH.fillEquipo(eqModel, equipos,null);
                        }
                    }
                    catch {
                        if (action == "equipo" || action == "all")
                        {
                            equipo equipos = new equipo();
                            equipos = pH.fillEquipo(eqModel, equipos,aud);                                                      
                        }
                    }
                }
                if (action == "climatizacion" || action == "all")
                {
                    climatizacion = pH.fillClimatizacion(cliModel, climatizacion);
                }
                if (action == "analisis" || action == "all")
                {
                    analisis = pH.fillAnalisis(anModel, analisis);
                }
                if (action == "transporte" || action == "all")
                {
                    transportes = pH.fillTransporte(trModel, transportes, code);
                }
                if (action == "agua" || action == "all")
                {
                    aguas = pH.fillAgua(agModel, aguas);
                }
                if (action == "aire" || action == "all")
                {
                    aires = pH.fillAire(aiModel, aires);
                }
                aj.SaveChanges();
                done = true;
            }
            else
            {
                auditoria au = pH.newAuditoria(sitio, dta);
                agua ag = new agua();
                analisis_cogeneracion an = new analisis_cogeneracion();
                climatizacion cl = new climatizacion();
                datos_transporte dt = new datos_transporte();
                equipo eq = new equipo();
                aire ai = new aire();

                if (action == "equipo" || action == "all")
                {
                    eq = pH.fillEquipo(eqModel, null,null);
                }
                eq.auditoria = au;
                if (action == "climatizacion" || action == "all")
                {
                    cl = pH.fillClimatizacion(cliModel, null);
                }
                cl.auditoria = au;
                if (action == "analisis" || action == "all")
                {
                    an = pH.fillAnalisis(anModel, null);
                }
                an.auditoria = au;
                if (action == "transporte" || action == "all")
                {
                    dt = pH.fillTransporte(trModel, null,code);
                }
                dt.auditoria = au;
                if (action == "agua" || action == "all")
                {
                    ag = pH.fillAgua(agModel, null);
                }
                ag.auditoria = au;
                if (action == "aire" || action == "all")
                {
                    ai = pH.fillAire(aiModel, null);
                }
                ai.auditoria = au;

                aj.auditorias.Add(au);
                aj.equipoes.Add(eq);
                aj.climatizacion.Add(cl);
                aj.analisis_cogeneracion.Add(an);
                aj.datos_transporte.Add(dt);
                aj.aguas.Add(ag);
                aj.aires.Add(ai);

                aj.SaveChanges();
                done = true;
            }
            return Json(done, JsonRequestBehavior.AllowGet);
        }        
    }
}
