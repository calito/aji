﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AJI.Controllers
{
    public class AjaxTestController : Controller
    {
        //ajidbEntities aj = new ajidbEntities();
        ajidbEntities1 aj = new ajidbEntities1();
        //Metodos aji
        [HttpPost]
        public JsonResult getEmpresas()
        {
            var queryEMPRESA = (from p in aj.empresas select new { 
                                    p.correo,
                                    p.fecha_ingreso,
                                    p.id_empresa,
                                    p.localizacion,
                                    p.logo,
                                    p.nombre,
                                    p.num_fiscal,
                                    p.num_sitios,
                                    p.representante,
                                    p.telefono                                     
                                }).OrderByDescending(p => p.fecha_ingreso).ToList();
            
            return Json(queryEMPRESA, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult getEmpresa(string par, string name)
        {
            if (par != "")
            {
                try
                {
                    string emp = par.Split('?')[1].Split('&')[1].Split('=')[1].Replace("%20", " ").ToString();
                    var queryEMPRESA = (from p in aj.empresas
                                        where p.nombre == emp
                                        select new
                                        {
                                            p.correo,
                                            p.fecha_ingreso,
                                            p.id_empresa,
                                            p.localizacion,
                                            p.logo,
                                            p.nombre,
                                            p.num_fiscal,
                                            p.num_sitios,
                                            p.representante,
                                            p.telefono
                                        }).OrderByDescending(p => p.fecha_ingreso).FirstOrDefault();

                    return Json(queryEMPRESA, JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    return Json("search", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                var querySitio = (from p in aj.empresas
                                  where p.nombre == name
                                  select new
                                        {
                                            p.correo,
                                            p.fecha_ingreso,
                                            p.id_empresa,
                                            p.localizacion,
                                            p.logo,
                                            p.nombre,
                                            p.num_fiscal,
                                            p.num_sitios,
                                            p.representante,
                                            p.telefono
                                        }).OrderByDescending(p => p.nombre).FirstOrDefault();

                return Json(querySitio, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult getSitios(string par)
        {
            string sit="";
            try { sit = par.Split('?')[1].Split('&')[1].Split('=')[1].Replace("%20", " ").ToString(); }
            catch { }
            if (sit == "")
            {
                var querySitio = (from p in aj.sitios
                                  select new { 
                                      p.area_utilizada,
                                      p.cogeneracion,
                                      p.compensacion_auto_consumo,
                                      p.consumo_electrico_anual,
                                      p.consumo_electrico_factura,
                                      p.direccion,
                                      p.extrangero,
                                      p.factor_mezcla,
                                      p.fecha_ingreso,
                                      p.geo_latitud,
                                      p.geo_longitud,
                                      p.instruccion,
                                      p.nombre_empresa,
                                      p.nombre_sitio,
                                      p.plazas_utilizadas,
                                      p.potencia_iluminacion,
                                      p.potencia_pc,
                                      p.propiedad,
                                      p.proveedor,
                                      p.suministrado_red,
                                      p.tiempo_completo,
                                      p.tiempo_parcial,
                                      p.tipo_uso                                      
                                  }).OrderByDescending(p => p.nombre_sitio).ToList();

                return Json(querySitio, JsonRequestBehavior.AllowGet);
            }
            else
            {

                var querySitio = (from p in aj.sitios
                                  where p.nombre_empresa == sit
                                  select new
                                  {
                                      p.area_utilizada,
                                      p.cogeneracion,
                                      p.compensacion_auto_consumo,
                                      p.consumo_electrico_anual,
                                      p.consumo_electrico_factura,
                                      p.direccion,
                                      p.extrangero,
                                      p.factor_mezcla,
                                      p.fecha_ingreso,
                                      p.geo_latitud,
                                      p.geo_longitud,
                                      p.instruccion,
                                      p.nombre_empresa,
                                      p.nombre_sitio,
                                      p.plazas_utilizadas,
                                      p.potencia_iluminacion,
                                      p.potencia_pc,
                                      p.propiedad,
                                      p.proveedor,
                                      p.suministrado_red,
                                      p.tiempo_completo,
                                      p.tiempo_parcial,
                                      p.tipo_uso   
                                  }).OrderByDescending(p => p.nombre_sitio).ToList();

                return Json(querySitio, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult getSitio(string par)
        {
            try
            {
                var paramsUrl = par.Split('?')[1].Split('&');
                string sit = paramsUrl[1].Split('=')[1].Replace("%20", " ").ToString();
                if (sit.Contains("nuevo/"))
                {
                    return Json(sit, JsonRequestBehavior.AllowGet);
                }
                var querySitio = (from p in aj.sitios
                                  where (p.nombre_empresa + "/" + p.nombre_sitio) == sit
                                  select new
                                  {
                                      p.area_utilizada,
                                      p.cogeneracion,
                                      p.compensacion_auto_consumo,
                                      p.consumo_electrico_anual,
                                      p.consumo_electrico_factura,
                                      p.direccion,
                                      p.extrangero,
                                      p.factor_mezcla,
                                      p.fecha_ingreso,
                                      p.geo_latitud,
                                      p.geo_longitud,
                                      p.instruccion,
                                      p.nombre_empresa,
                                      p.nombre_sitio,
                                      p.plazas_utilizadas,
                                      p.potencia_iluminacion,
                                      p.potencia_pc,
                                      p.propiedad,
                                      p.proveedor,
                                      p.suministrado_red,
                                      p.tiempo_completo,
                                      p.tiempo_parcial,
                                      p.tipo_uso   
                                  }).OrderByDescending(p => p.nombre_sitio).FirstOrDefault();

                return Json(querySitio, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                {
                    return Json("search", JsonRequestBehavior.AllowGet);
                }
            }
        }
        [HttpPost]
        public JsonResult getAuditorias(string par, string aud)
        {
            string sit = "";
            try { sit = par.Split('?')[1].Split('&')[1].Split('=')[1].Replace("%20", " ").ToString(); }
            catch { }
            if (sit == "")
            {
                var querySitio = (from p in aj.auditorias
                                  where p.nombre_auditor == aud
                                  select new
                                  {
                                      p.codigo,
                                      p.descripcion,
                                      p.fecha_ingreso ,
                                      p.nombre_auditor,
                                      p.sitio.nombre_sitio,
                                      p.sitio.nombre_empresa
                                  }).OrderByDescending(p => p.fecha_ingreso).ToList();

                return Json(querySitio, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var querySitio = (from p in aj.auditorias
                                  where p.nombre_auditor == aud
                                  && (p.sitio.nombre_empresa == sit ||
                                  p.sitio.nombre_sitio == sit)
                                  select new
                                  {
                                      p.codigo,
                                      p.descripcion,
                                      p.fecha_ingreso,
                                      p.nombre_auditor,
                                      p.sitio.nombre_sitio,
                                      p.sitio.nombre_empresa
                                  }).OrderByDescending(p => p.fecha_ingreso).ToList();

                return Json(querySitio, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult getAuditoria(string code)
        {
            try
            {
                var querySitio = (from p in aj.auditorias
                                  where p.codigo == code
                                  select new
                                  {
                                      p
                                  }).FirstOrDefault();
                var cod = querySitio.p.codigo;
                var desc = querySitio.p.descripcion;
                var fecha = querySitio.p.fecha_ingreso;
                var nom = querySitio.p.nombre_auditor;
                var sit = querySitio.p.sitio.nombre_sitio;
                var emp = querySitio.p.sitio.empresa.nombre;
                var logo = querySitio.p.sitio.empresa.logo;
                //valores
                var cogeneracion = querySitio.p.sitio.cogeneracion;
                var areautilizada = querySitio.p.sitio.area_utilizada;

                var tranToProm = querySitio.p.datos_transporte.FirstOrDefault().transportes;

                string ConsumoPromediaGasolina = (double.Parse(tranToProm.Where(
                    a => a.tipo_combustible == "Gasolina" ).Sum(a=>a.consumo_combustible).ToString())/
                    tranToProm.Where(a=>a.tipo_combustible == "Gasolina" ).Count()).ToString();
                if (ConsumoPromediaGasolina == "NeuN") ConsumoPromediaGasolina = "0";

                string ConsumoPromediaDiesel = (double.Parse(tranToProm.Where(
                    a => a.tipo_combustible == "Diesel").Sum(a => a.consumo_combustible).ToString())/
                    tranToProm.Where(a=>a.tipo_combustible == "Diesel").Count()).ToString();
                if (ConsumoPromediaDiesel == "NeuN") ConsumoPromediaDiesel = "0";

                var empleadosSitio = (int.Parse(querySitio.p.sitio.tiempo_completo.ToString())
                    + int.Parse(querySitio.p.sitio.tiempo_parcial.ToString())); 
                //
                var ag = querySitio.p.aguas.Select(a => new
                {
                    a.agua_caliente,
                    a.agua_fria
                }).FirstOrDefault();
                var ai = querySitio.p.aires.Select(a => new
                {
                    a.consumo_aire_comprimido,
                    a.consumo_compresores
                }).FirstOrDefault();
                var an = querySitio.p.analisis_cogeneracion.Select(a => new
                {
                    a.costo_clima_distancia_año,
                    a.costo_prom_combustible
                }).FirstOrDefault();
                var cl = querySitio.p.climatizacion.Select(a => new
                {
                    a.climatizacion_dist,
                    a.combustible_climatizacion,
                    a.combustible_cogenerazion,
                    a.perdidas,
                    a.sistema_clima_distancia,
                    a.suministro_cogeneracion_año,
                    a.tipo_climatizacion,
                    a.trans_energia_electrica,
                    a.trans_energia_termica,
                    a.valor_aceite_combustible,
                    a.valor_gas_natural,
                    a.valor_pellets_madera,
                }).FirstOrDefault();
                var da = querySitio.p.datos_transporte.Select(a => new {
                    a.consumo_disel_año,
                    a.consumo_energia_recorido,
                    a.consumo_gasolina_año,
                    a.consumo_kerosene,
                    a.costo_disel,
                    a.costo_gasolina,
                    a.costo_viaje_avion,
                    a.costo_viaje_tren,
                    a.valor_calorifico_kerosene,
                    a.viaje_avion,
                    a.viaje_tren,                    
                }).FirstOrDefault();
                var eq = querySitio.p.equipoes.Select(a => new { 
                    a.cantidad,
                    a.id_equipo,
                    a.descripcion,
                    a.potencia,
                    a.departamento
                }).ToList();
                var tr = querySitio.p.datos_transporte.FirstOrDefault().transportes.Select(a => new
                {
                    a.id_transporte,
                    a.marca_vehiculo,
                    a.placa_vehiculo,
                    a.tipo_combustible,
                    a.consumo_combustible
                });
                return Json(new
                {
                    cod,
                    desc,
                    fecha,
                    nom,
                    sit,
                    emp,
                    logo,
                    ag,
                    ai,
                    an,
                    cl,
                    da,
                    eq,
                    tr,
                    cogeneracion,
                    areautilizada,
                    ConsumoPromediaGasolina,
                    ConsumoPromediaDiesel,
                    empleadosSitio
                }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                {
                    return Json("search", JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public JsonResult BigGraphic()
        {
            var dt = (from a in aj.datos_transporte
                     select a).ToList();
            //dt.FirstOrDefault().transportes.FirstOrDefault().consumo_combustible;
            string st = "{ x: 'USA', y: 50 }, { x: 'China', y: 40 }";
                            
            return Json(st, JsonRequestBehavior.AllowGet);
        }
        //operaciones en equipos
        [HttpPost]
        public JsonResult getEquipo(string id_equipo)
        {
            int idInt = int.Parse(id_equipo);
            var dt = (from eq in aj.equipoes
                      where eq.id_equipo == idInt
                      select new
                      {
                          eq.cantidad,
                          eq.consumo,
                          eq.departamento,
                          eq.descripcion,
                          eq.horas_uso,
                          eq.observacion,
                          eq.potencia,
                          eq.tipo.nombre,
                          eq.uso,
                          eq.id_equipo
                      }).FirstOrDefault();
            return Json(dt, JsonRequestBehavior.AllowGet);
        }
        //operaciones en transporte
        [HttpPost]
        public JsonResult getTransporte(string id_transporte)
        {
            int idInt = int.Parse(id_transporte);
            var dt = (from eq in aj.transportes
                      where eq.id_transporte == idInt
                      select new
                      {
                          eq.id_transporte,
                          eq.marca_vehiculo,
                          eq.placa_vehiculo,
                          eq.tipo_combustible,
                          eq.consumo_combustible,
                      }).FirstOrDefault();
            return Json(dt, JsonRequestBehavior.AllowGet);
        }
    }
}
