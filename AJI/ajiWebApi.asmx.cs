﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//using System.Web.Http.Cors;
using System.Web.Services;  
using System.Web.Script.Services;  
using System.Web.Script.Serialization;

namespace AJI{
//{
    //[EnableCors(origins: "http://localhost:7843/ajiWebApi.asmx", headers: "*", methods: "*")]
    //[WebService]
    //public class ajiWebApi : System.Web.Services.WebService
    //{
    [WebService]
    public class ajiWebApi
    {
        [WebMethod]
        public Employee[] GetEmployessXML()
        {
            Employee[] emps = new Employee[] {  
            new Employee(){Id=101,Name="Nitin",Salary=10000},  
            new Employee(){Id=102,Name="Dinesh",Salary=100000}
            };
            return emps;
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetEmployessJSON()
        {
            Employee[] emps = new Employee[] {  
            new Employee()  
            {  
                Id=101,  
                Name="Nitin",  
                Salary=10000  
            },  
            new Employee()  
            {  
                Id=102,  
                Name="Dinesh",  
                Salary=100000  
            }  
        };
            return new JavaScriptSerializer().Serialize(emps);
        }
    }
    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Salary { get; set; }
    }
}
