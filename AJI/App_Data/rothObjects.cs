﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AJI
{
    public class AJIobjects
    {
    }
    public class clAuditoria
    {
        public string fecha_ingreso = "";
        public string nombre_auditor = "";
        public string descripcion = "";
    }
    public class clCompany
    {
        public string user = "";
        public string correo = "";
        public string fecha_ingreso = "";
        public string localizacion = "";
        public string logo = "";
        public string nombre = "";
        public string nombreNuevo = "";
        public string num_fiscal = "";
        public string representante = "";
        public string telefono = "";
    }
    //, , dataAire, dataAgua
    public class clAgua
    {
        public string agua_fria = "";
        public string agua_caliente = "";
    }
    public class clAire
    {
        public string consumo_compresores = "";
        public string consumo_aire_comprimido = "";
    }
    public class clTransporte
    {
        public string consumo_disel_an = "";
        public string costo_disel = "";
        public string consumo_gasolina_an = "";
        public string costo_gasolina = "";
        public string viaje_tren = "";
        public string consumo_energia_recorido = "";
        public string costo_viaje_tren = "";
        public string viaje_avion = "";
        public string consumo_kerosene = "";
        public string valor_calorifico_kerosene = "";
        public string costo_viaje_avion = "";
        /*equipo de transporte*/
        public string _clT_Id_vehiculo = "";
        public string _clT_marca_vehiculo = "";
        public string _clT_placa_vehiculo = "";
        public string _clT_tipo_combustible = "";
        public string _clT_consumo_combustible = "";
    }
    public class clAnalisis
    {
        public string costo_prom_combustible = "";
        public string costo_clima_distancia = "";
    }
    public class clClimatizacion
    {
        public string Cl_tipo = "";
        public string climatizacion_dist = "";
        public string valor_gas_natural = "";
        public string trans_energia_electrica = "";
        public string trans_energia_termica = "";
        public string cl_perdidas = "";
        public string combustible_climatizacion = "";
        public string valor_aceite_combustible = "";
        public string valor_pellets_madera = "";
        public string sistema_clima_distancia = "";
        public string suministro_cogeneracion_año = "";
        public string combustible_cogenerazion = "";
    }
    public class clEquipos
    {
        public string id_Equipo = "";
        public string descripcion = "";
        public string departamento = "";
        public string uso = "";
        public string potencia = "";
        public string cantidad = "";
        public string horas_uso = "";
        public string consumo = "";
        public string observacion = "";
        public string eq_tipo = "";
        public string rango = "";
        public string costo_energia = "";
        public string dias_trabajo = "";
    }
    public class clSites
    {
        public string nombreEmpresaParaSitio = "";
        public string nombreSitio = "";
        public string nombreSitioViejo = "";
        public string tipoUso = "";
        public string areaUtilizada = "";
        public string Propiedad = "";
        public string direccion = "";
        public string geoLatitud = "";
	    public string geoLongitud = "";
        public string tiempoCompleto = "";
        public string tiempoParcial = "";
        public string extrangero = "";
        public string instruccion = "";
        public string potenciaDeComputadora = "";
        public string potenciaDeIluminacion = "";
        public string plazasUtilizadas = "";
        public string proveedor = "";
        public string factorMezcla = "";
        public string consumoElectricoAnual = "";
        public string suministradoRed = "";
        public string cogeneracion = "";
        public string consumoElectricoFactura = "";
        public string compensacionAutoconsumo = "";
    }

    //aji end 


    public class mainTable
    {
        public string SHORT_NAME{get;set;}
        public string ACCOUNT_REP_NAME{get;set;}
        public decimal PRINCIPAL_SIGNE{get;set;}
        public decimal COMMISSION{get;set;}
    }
    public class SumsTable
    {
        public string SEGURITY_DESCRIPTION{ get; set; }
        public int AE{ get; set; }
        public string SYMBOL{ get; set; }
        public string TRADE_REP_NAME { get; set; }
        public string BULL_SELL{ get; set; }
        public decimal QUANTITY{ get; set; }
        public decimal PRICE { get; set; }
        public decimal PRINCIPAL_SIGNE { get; set; }
        public decimal COMMISSION { get; set; }
        public string TRADE_DATE{ get; set; }
    }
    public class detailTable
    {
        public string ACCOUNT { get; set; }
        public string SEGURITY_DESCRIPTION { get; set; }
        public string SHORT_NAME { get; set; }
        public string AE { get; set; }
        public string SYMBOL { get; set; }
        public string ACCOUNT_REP_NAME { get; set; }
        public string BULL_SELL { get; set; }
        public string QUANTITY { get; set; }
        public string PRICE { get; set; }
        public string PRINCIPAL_SIGNE { get; set; }
        public string COMMISSION { get; set; }
        public string TRADE_DATE { get; set; }
        public string NET_MONEY { get; set; }
    }
}