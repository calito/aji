﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace AJI
{
    public class ProcessToHelp
    {
        ajidbEntities1 aj = new ajidbEntities1();
        public auditoria newAuditoria(string sitio, clAuditoria au)
        {
            auditoria auNew = new auditoria();
            auNew.id_sitio = int.Parse((from a in aj.sitios where a.nombre_sitio == sitio select a.id_sitio).FirstOrDefault().ToString());
            auNew.fecha_ingreso = DateTime.Parse(au.fecha_ingreso);
            auNew.nombre_auditor = au.nombre_auditor;
            auNew.descripcion = au.descripcion;
            var codigo = (from a in aj.sitios
                             where a.nombre_sitio == sitio
                             select new { a.nombre_empresa, a.nombre_sitio}).FirstOrDefault();
            auNew.codigo = CreateMD5(codigo.nombre_empresa + codigo.nombre_sitio + DateTime.Now.ToLongDateString() + DateTime.Now.ToLongTimeString());
            return auNew;
        } 
        public equipo fillEquipo(clEquipos eqModel,equipo equiposObj,auditoria aud ) 
        {            
            tipo tp_eq = new tipo();
            equiposObj.descripcion = eqModel.descripcion;
            equiposObj.departamento = eqModel.departamento;
            equiposObj.uso = eqModel.uso;
            try { equiposObj.potencia = int.Parse(eqModel.potencia); }
            catch { }
            try { equiposObj.cantidad = int.Parse(eqModel.cantidad); }
            catch { }
            try { equiposObj.horas_uso = int.Parse(eqModel.horas_uso); }
            catch { }
            try { equiposObj.consumo = int.Parse(eqModel.consumo); }
            catch { }
            equiposObj.observacion = eqModel.observacion;
            tp_eq = aj.tipoes.Where(a => a.nombre == eqModel.eq_tipo).Select(a => a).FirstOrDefault();
            if (tp_eq != null) {
                equiposObj.tipo.id_tipo = tp_eq.id_tipo;
            }
            else
            {
                tipo newTipo = new tipo();
                newTipo.nombre = eqModel.eq_tipo;
                aj.tipoes.Add(newTipo);
                equiposObj.tipo = newTipo;
            }
            try { equiposObj.rango = int.Parse(eqModel.rango); }
            catch { equiposObj.rango = 0; }
            try { equiposObj.costo_energia = int.Parse(eqModel.costo_energia); }
            catch { equiposObj.costo_energia = 0; }
            try { equiposObj.dias_trabajo = int.Parse(eqModel.dias_trabajo); }
            catch { equiposObj.dias_trabajo = 0; }
            try { equiposObj.cantidad = int.Parse(eqModel.cantidad); }
            catch { equiposObj.cantidad = 0; }
            if (aud != null)
            {
                var au = (from a in aj.auditorias
                         where a.id_auditoria == aud.id_auditoria
                         select a).FirstOrDefault();
                equiposObj.auditoria = au;
                aj.equipoes.Add(equiposObj);
                aj.SaveChanges();
                
            }
            return equiposObj;
        }
        public climatizacion fillClimatizacion(clClimatizacion cliModel,climatizacion climatizacionObj)
        {
            if (climatizacionObj == null)
            { climatizacionObj = new climatizacion(); }
            try { climatizacionObj.climatizacion_dist = bool.Parse(cliModel.climatizacion_dist); }
            catch { }
            climatizacionObj.combustible_climatizacion = cliModel.combustible_climatizacion;
            climatizacionObj.combustible_cogenerazion = cliModel.combustible_cogenerazion;
            try { climatizacionObj.perdidas = decimal.Parse(cliModel.cl_perdidas); }
            catch { }
            try { climatizacionObj.sistema_clima_distancia = decimal.Parse(cliModel.sistema_clima_distancia); }
            catch { }
            try { climatizacionObj.suministro_cogeneracion_año = decimal.Parse(cliModel.suministro_cogeneracion_año); }
            catch { }
            climatizacionObj.tipo_climatizacion = cliModel.Cl_tipo;
            try { climatizacionObj.trans_energia_electrica = decimal.Parse(cliModel.trans_energia_electrica); }
            catch { }
            try { climatizacionObj.trans_energia_termica = decimal.Parse(cliModel.trans_energia_termica); }
            catch { }
            try { climatizacionObj.valor_aceite_combustible = decimal.Parse(cliModel.valor_aceite_combustible); }
            catch { }
            try { climatizacionObj.valor_gas_natural = decimal.Parse(cliModel.valor_gas_natural); }
            catch { }
            try { climatizacionObj.valor_pellets_madera = decimal.Parse(cliModel.valor_pellets_madera); }
            catch { }
            return climatizacionObj;
        }
        public analisis_cogeneracion fillAnalisis(clAnalisis anModel, analisis_cogeneracion analisisObj)
        {
            if (analisisObj == null)
            { analisisObj = new analisis_cogeneracion(); }
            try { analisisObj.costo_clima_distancia_año = double.Parse(anModel.costo_clima_distancia); }
            catch { }
            try { analisisObj.costo_prom_combustible = double.Parse(anModel.costo_prom_combustible); }
            catch { }
            return analisisObj;
        }
        public datos_transporte fillTransporte(clTransporte trModel, datos_transporte transportesObj,string code)
        {
            if (transportesObj == null)
            { transportesObj = new datos_transporte(); }

            try { transportesObj.consumo_disel_año = double.Parse(trModel.consumo_disel_an); }
            catch { }
            try { transportesObj.consumo_energia_recorido = double.Parse(trModel.consumo_energia_recorido); }
            catch { }
            try { transportesObj.consumo_gasolina_año = double.Parse(trModel.consumo_gasolina_an); }
            catch { }
            try { transportesObj.consumo_kerosene = double.Parse(trModel.consumo_kerosene); }
            catch { }
            try { transportesObj.costo_disel = double.Parse(trModel.costo_disel); }
            catch { }
            try { transportesObj.costo_gasolina = double.Parse(trModel.costo_gasolina); }
            catch { }
            try { transportesObj.costo_viaje_avion = double.Parse(trModel.costo_viaje_avion); }
            catch { }
            try { transportesObj.costo_viaje_tren = double.Parse(trModel.costo_viaje_tren); }
            catch { }
            try { transportesObj.valor_calorifico_kerosene = double.Parse(trModel.valor_calorifico_kerosene); }
            catch { }
            try { transportesObj.viaje_avion = double.Parse(trModel.viaje_avion); }
            catch { }
            try { transportesObj.viaje_tren = double.Parse(trModel.viaje_tren); }
            catch { }

            //transporte para datos_transporte
            transporte tr_dt = new transporte();
            try
            {
                int id = int.Parse(trModel._clT_Id_vehiculo);
                tr_dt = transportesObj.transportes.Where(a => a.id_transporte == id).Select(a => a).FirstOrDefault();
            }
            catch { }
            if (tr_dt != null && tr_dt.id_dato_trans != null)
            {
                tr_dt.marca_vehiculo = trModel._clT_marca_vehiculo;
                tr_dt.placa_vehiculo = trModel._clT_placa_vehiculo;
                tr_dt.tipo_combustible = trModel._clT_tipo_combustible;
                try { tr_dt.consumo_combustible = double.Parse(trModel._clT_consumo_combustible); }
                catch { }
            }
            else
            {
                if (trModel._clT_consumo_combustible != "" &&
                    trModel._clT_marca_vehiculo != ""&&
                    trModel._clT_placa_vehiculo != "" &&
                    trModel._clT_tipo_combustible != "" )
                {
                    transporte newTrans = new transporte();
                    try { newTrans.consumo_combustible = double.Parse(trModel._clT_consumo_combustible); }
                    catch { }

                    newTrans.marca_vehiculo = trModel._clT_marca_vehiculo;
                    newTrans.placa_vehiculo = trModel._clT_placa_vehiculo;
                    newTrans.tipo_combustible = trModel._clT_tipo_combustible;
                    newTrans.datos_transporte = transportesObj;
                    try
                    {
                        transportesObj.transportes.Add(newTrans);
                    }
                    catch { }
                }
            }
            return transportesObj;
        }
        public agua fillAgua(clAgua agModel, agua aguaObj)
        {
            if (aguaObj == null)
            { aguaObj = new agua(); }
            try { aguaObj.agua_caliente = double.Parse(agModel.agua_caliente); }
            catch { }
            try { aguaObj.agua_fria = double.Parse(agModel.agua_fria); }
            catch { }
            return aguaObj;
        }
        public aire fillAire(clAire aiModel, aire aireObj )
        {
            if (aireObj == null)
            { aireObj = new aire(); }
            try { aireObj.consumo_aire_comprimido = decimal.Parse(aiModel.consumo_aire_comprimido); }
            catch { }
            try { aireObj.consumo_compresores = decimal.Parse(aiModel.consumo_compresores); }
            catch { }
            return aireObj;
        }
        public string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hashBytes = md5.ComputeHash(inputBytes);

            // Convert the byte array to hexadecimal string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }    
}