﻿
function setClimatizacion(cl) {
    $("#tipoClimatizacion").val(cl.tipo_climatizacion);
    $("#combustibleCogeneracion").val(cl.combustible_cogenerazion);
    $("#combustibleClimatizacion").val(cl.combustible_climatizacion);
    $('input[name="climDist"][value="' + cl.climatizacion_dist + '"]').attr('checked', true);
    valorCalorificoAceiteCombustible.value = cl.valor_aceite_combustible;
    valorCalorificoGasNatural.value = cl.valor_gas_natural;
    valorCalorificoPelletsMadera.value = cl.valor_pellets_madera;
    sistemaClimatizacionDistacia.value = cl.sistema_clima_distancia;
    suministroCogeneracion.value = cl.suministro_cogeneracion_año;
    transformacionEnergiaElectrica.value = cl.trans_energia_electrica;
    transformacionEnergiaTermica.value = cl.trans_energia_termica;
    Perdidas.value = cl.perdidas;
}
function setAnalisis(an){
    costosPromedioCombustible.value = an.costo_prom_combustible;
    costosClimatizacionDistancia.value = an.costo_clima_distancia_año;
}
function setDatostransporte(da){
    consumoTotalDieselAunual.value = da.consumo_disel_año;
    consumoEnergiaRecorrido.value = da.consumo_energia_recorido;
    consumoTotalGasolinaAnual.value = da.consumo_gasolina_año;
    consumoKerosene.value = da.consumo_kerosene;
    costosDiesel.value = da.costo_disel;
    costosGasolina.value = da.costo_gasolina;
    costosViajesAvion.value = da.costo_viaje_avion;
    costosViajeTren.value = da.costo_viaje_tren;
    valorCaloríficoKerosene.value = da.valor_calorifico_kerosene;
    viajesAvion.value = da.viaje_avion;
    viajesTren.value = da.viaje_tren;
}
function setAgua(ag) {
    aguaCaliente.value = ag.agua_caliente;
    aguaFria.value = ag.agua_fria;
}
function setAire(ai) {
    consumoAireComprimido.value = ai.consumo_aire_comprimido;
    consumoCompresor.value = ai.consumo_compresores;
}
