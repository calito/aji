﻿//D40 + D41
function EmpleadosSitioOP() {
    EmpleadosSitio.innerHTML = (
        parseFloat(document.getElementById("tiempoCompleto").value) +
        parseFloat(document.getElementById("tiempoParcial").value)) +
        "<span>empleados en Sitio</span>";
}
function EmpleadosSitioUPDOP() {
    EmpleadosSitioUPD.innerHTML = (
        parseFloat(document.getElementById("tiempoCompletoUPD").value) +
        parseFloat(document.getElementById("tiempoParcialUPD").value)) +
        "<span>empleados en Sitio</span>";
}
//(D70 + D71) / 1000 * D72 * 8 * D73
function ConsumoElectricoOP() {
    ConsumoElectrico.innerHTML = ((
        parseFloat(document.getElementById("potenciaDeComputadora").value) +
        parseFloat(document.getElementById("potenciaDeIluminacion").value))
        / 1000 * document.getElementById("plazasUtilizadas").value * 8 * 250) +
        "<span style='display: inline;'>kWh/a</span><span>Consumo eléctrico de plaza de trabajo</span>";
}
function ConsumoElectricoUPDOP() {
    ConsumoElectricoUPD.innerHTML = ((
        parseFloat(document.getElementById("potenciaDeComputadoraUPD").value) +
        parseFloat(document.getElementById("potenciaDeIluminacionUPD").value))
        / 1000 * document.getElementById("plazasUtilizadasUPD").value * 8 * 250) +
        "<span style='display: inline;'>kWh/a</span><span>Consumo eléctrico de plaza de trabajo</span>";
}

//D54+D55+D56
function CTConsumoElectricoOP() {
    var costoCongeneracion= parseFloat(document.getElementById("costosCogeneracion").innerHTML.replace("Costos Totales Consumo Electrico", "")); 
    if(!isNaN(costoCongeneracion)){
    }else {
        costoCongeneracion = 0;        
    }
    CTConsumoElectrico.innerHTML = "€ " + (
        parseFloat(document.getElementById("consumoElectricoFactura").value) +
        parseFloat(document.getElementById("compensacionAutoconsumo").value) +
        costoCongeneracion) + "<span>Costos Totales Consumo Eléctrico</span>";
}
function CTConsumoElectricoUPDOP() {
    var costoCongeneracion = parseFloat(document.getElementById("costosCogeneracionUPD").innerHTML.replace("Costos Totales Consumo Electrico", ""));
    if (!isNaN(costoCongeneracion)) {
    } else {
        costoCongeneracion = 0;
    }
    CTConsumoElectricoUPD.innerHTML = "€ " + (
        parseFloat(document.getElementById("consumoElectricoFacturaUPD").value) +
        parseFloat(document.getElementById("compensacionAutoconsumoUPD").value) +
        costoCongeneracion) + "<span>Costos Totales Consumo Eléctrico</span>";
}